__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2019, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

import json
import uuid
import webbrowser
# these imports are just needed in this example
from builtins import input

from id4me_rp_client import *

registrations = dict()

# a routine to save client registration at authority
def save_authority_registration(auth_name, auth_content):
    registrations[auth_name] = auth_content
    pass


# a routine to load client registration at authority
def load_authority_registration(auth_name):
    return registrations[auth_name]

# create client object with basic parameters of your app
client = ID4meClient(
    get_client_registration=load_authority_registration,
    save_client_registration=save_authority_registration,
    app_type=OIDCApplicationType.web,
    validate_url='https://dynamicdns.domainconnect.org/ddnscode',
    client_name='Foo app',
    private_jwks_json=ID4meClient.generate_new_private_keys_set())

try:
    # make a discovery of identity authority and register if needed
    # find_authority and save_authority are optional, but when missing client will be registered each time anew
    ctx = client.get_rp_context(
        id4me='idtest1.domainid.community')

    # get a link to login routine
    link = client.get_consent_url(
        ctx,
        claimsrequest=ID4meClaimsRequest(
            userinfo_claims={
                OIDCClaim.name: ID4meClaimRequestProperties(reason='To call you by name'),
                OIDCClaim.email: ID4meClaimRequestProperties(essential=True, reason='To be able to contact you'),
                OIDCClaim.email_verified: ID4meClaimRequestProperties(reason='To know if your E-mail was verified'),
            }),
        state=str(uuid.uuid4())
        )
    print('Please open the link:\n{}'.format(link))
    print('\nPassword: WU9ZOZGC0DgzS9blLECd')

    webbrowser.open(link, autoraise=True)

    # Normally code will arrive as query param on client.validateUrl
    code = input('Please enter code: ')
    # Get ID token
    id_token = client.get_idtoken(context=ctx, code=code)
    print('ID Token:\n{}'.format(json.dumps(id_token, sort_keys=True, indent=4)))
    # Get User Info
    userinfo = client.get_user_info(context=ctx)
    print('User Info:\n{}'.format(json.dumps(userinfo, sort_keys=True, indent=4)))
except ID4meException as e:
    print('Exception: {}'.format(e))
