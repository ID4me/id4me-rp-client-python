__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2019, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

from unittest import mock
from unittest.mock import MagicMock

from unittest2 import TestCase

from id4me_rp_client.network import *


# solution from https://stackoverflow.com/questions/21611559/assert-that-a-method-was-called-with-one-argument-out-of-several
def Any(cls):
    class Any(cls):
        def __eq__(self, other):
            return isinstance(other, cls)
    return Any()

class TestHttp_request(TestCase):
    def test_http_request__http(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext()
            res, code, _ = http_request(context, "GET", "http://foo.com")
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, None, {})
            assert res == "Response"
            assert code == 200

    def test_http_request__http_with_proxy(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            connection_mock.set_tunnel = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext(proxy_host='host', proxy_port='port')
            res, code, _ = http_request(context, "GET", "http://foo.com")
            httpclient_mock.HTTPConnection.assert_called_with('host', 'port')
            connection_mock.set_tunnel.assert_called_with('foo.com')

            connection_mock.request.assert_called_with('GET', None, None, {})
            assert res == "Response"
            assert code == 200


    def test_http_request__https(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            httpclient_mock.HTTPSConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext()
            res, code, _ = http_request(context, "GET", "https://foo.com")
            # solution from https://stackoverflow.com/questions/21611559/assert-that-a-method-was-called-with-one-argument-out-of-several
            httpclient_mock.HTTPSConnection.assert_called_with('foo.com', context=Any(ssl.SSLContext))
            connection_mock.request.assert_called_with('GET', None, None, {})
            assert res == "Response"
            assert code == 200

    def test_http_request__https_with_proxy(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            httpclient_mock.HTTPSConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            connection_mock.set_tunnel = MagicMock()
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext(proxy_host='host', proxy_port='port')
            res, code, _ = http_request(context, "GET", "https://foo.com")
            # solution from https://stackoverflow.com/questions/21611559/assert-that-a-method-was-called-with-one-argument-out-of-several
            httpclient_mock.HTTPSConnection.assert_called_with('host', 'port', context=Any(ssl.SSLContext))
            connection_mock.set_tunnel.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, None, {})
            assert res == "Response"
            assert code == 200


    def test_http_request__invalidurl(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            httpclient_mock.HTTPSConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext()
            try:
                http_request(context, "GET", "ftp://foo.com")
                assert False, 'Exception expected but not happened'
            except:
                pass

    def test_http_request__allparams(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 201

            context = NetworkContext()
            http_request(context,
                                     "GET",
                                     "http://foo.com",
                                     body='Test body',
                                     basic_auth=('user', 'password'),
                                     bearer='baarer_token',
                                     content_type='application/text',
                                     accepts='application/json',
                                     cache_control='no-cache',
                                     accepted_statuses=[200,201],
                                     max_bytes=5000)
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, 'Test body',
                                                       {'Authorization': 'Bearer baarer_token',
                                                        'Content-Type': 'application/text',
                                                        'Accept': 'application/json',
                                                        'Cache-Control': 'no-cache'}
                                                       )

    def test_http_request__error_code(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 500

            context = NetworkContext()
            try:
                http_request(context, "GET", "http://foo.com")
                assert False, "Expected exception ain't happen"
            except Exception as e:
                pass

    def test_http_request__http_with_redirect(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock

            response1_mock = MagicMock()
            response1_mock.read.return_value = "1st Response".encode()
            response1_mock.status = 301
            response1_mock.getheader.return_value = 'http://bar.com/redirected'

            response2_mock = MagicMock()
            response2_mock.read.return_value = "2nd Response".encode()
            response2_mock.status = 200

            connection_mock.getresponse = MagicMock(side_effect = [response1_mock, response2_mock])

            context = NetworkContext()
            res, code, _ = http_request(context, "GET", "http://foo.com")
            httpclient_mock.HTTPConnection.assert_called_with('bar.com')
            connection_mock.request.assert_called_with('GET', '/redirected', None, {})
            assert res == "2nd Response"
            assert code == 200

    def test_http_request__http_too_long(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext(max_bytes=8)
            try:
                http_request(context, "GET", "http://foo.com")
                assert False, 'No exception on too long response'
            except:
                pass
            response_mock.read.assert_called_with(amt=8)
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, None, {})

    def test_post_data(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 200

            context = NetworkContext()
            ret = post_data(context, "http://foo.com", "DATA")
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('POST', None, 'DATA', {'Content-Type': 'application/x-www-form-urlencoded'})
            assert ret == "Response"

    def test_post_data__with_accepted_statuses(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 404

            context = NetworkContext()
            ret = post_data(context, "http://foo.com", "DATA", accepted_statuses=[200, 404])
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('POST', None, 'DATA', {'Content-Type': 'application/x-www-form-urlencoded'})
            assert ret == "Response"

    def test_post_data__error_code(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 500

            context = NetworkContext()
            try:
                post_data(context, "http://foo.com", "DATA")
                assert False, 'Exception not triggered for an invalid code'
            except:
                pass
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('POST', None, 'DATA', {'Content-Type': 'application/x-www-form-urlencoded'})

    def test_post_json__with_accepted_statuses(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 404

            context = NetworkContext()
            ret = post_json(context, "http://foo.com", {'foo': 'bar'}, accepted_statuses=[200, 404])
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('POST', None, '{"foo": "bar"}', {'Content-Type': 'application/json'})
            assert ret == "Response"

    def test_post_json__error_code(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = "Response".encode()
            response_mock.status = 500

            context = NetworkContext()
            try:
                post_json(context, "http://foo.com", {'foo': 'bar'})
                assert False, 'Exception not triggered for an invalid code'
            except:
                pass
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('POST', None, '{"foo": "bar"}', {'Content-Type': 'application/json'})

    def test_get_json_auth__with_accepted_statuses(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = json.dumps({'foo': 'bar'}).encode()
            response_mock.status = 404

            context = NetworkContext()
            ret = get_json_auth(context, "http://foo.com", basic_auth=('foo', 'bar'), accepted_statuses=[200, 404])
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, None, {'Authorization': 'Basic Zm9vOmJhcg=='})
            assert ret == {'foo': 'bar'}

            ret = get_json_auth(context, "http://foo.com/bar", bearer ='foo', accepted_statuses=[200, 404])
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', '/bar', None, {'Authorization': 'Bearer foo'})
            assert ret == {'foo': 'bar'}


    def test_get_json_auth__error_code(self):
        with mock.patch('id4me_rp_client.network.client') as httpclient_mock:
            connection_mock = MagicMock()
            connection_mock.request = MagicMock()
            httpclient_mock.HTTPConnection.return_value = connection_mock
            response_mock = MagicMock()
            connection_mock.getresponse.return_value = response_mock
            response_mock.read.return_value = json.dumps({'foo': 'bar'}).encode()
            response_mock.status = 500

            context = NetworkContext()
            try:
                get_json_auth(context, "http://foo.com", basic_auth=('foo', 'bar'), accepted_statuses=[200, 404])
                assert False, 'Exception not triggered for an invalid code'
            except:
                pass
            httpclient_mock.HTTPConnection.assert_called_with('foo.com')
            connection_mock.request.assert_called_with('GET', None, None, {'Authorization': 'Basic Zm9vOmJhcg=='})
