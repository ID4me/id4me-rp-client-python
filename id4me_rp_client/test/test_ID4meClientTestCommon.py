__author__ = "Pawel Kowalik"
__copyright__ = "Copyright 2018, 1&1 IONOS SE"
__credits__ = []
__license__ = "MIT"
__maintainer__ = "Pawel Kowalik"
__email__ = "pawel-kow@users.noreply.github.com"
__status__ = "Beta"

from unittest2 import TestCase

from id4me_rp_client import *


class TestID4meClientTestCommon(TestCase):
    test_host = 'localhost'
    test_port = 8090

    @staticmethod
    def _get_test_client(get_client_registration, save_client_registration=None, params=None, private_jwks_json = None,
                         generate_private_keys = True, **kwargs):
        """
        Function generates a default test client with given parameter set
        :param get_client_registration: func(str)->str
        :type get_client_registration: funtion to get client registration from store
        :param save_client_registration: func(str, str)
        :type save_client_registration: funtion to save client registration into store
        :param params: additional parameters as dictionary. currently supported 'requireencryption'
        :type params: dict
        :param private_jwks_json: string representation of private key jwks
        :type private_jwks_json: str
        :param generate_private_keys: if private encryption keys shall be generated automatically
        :type generate_private_keys: bool
        :return: Pre-configured test client
        :rtype: ID4meClient
        """
        if params is None:
            params = dict()
        return ID4meClient(
            get_client_registration=get_client_registration,
            save_client_registration=save_client_registration,
            validate_url='http://{}:{}/validate'.format(TestID4meClientTestCommon.test_host, TestID4meClientTestCommon.test_port),
            #jwks_url='http://{}:{}/jwks'.format(TestID4meClientTestCommon.test_host, TestID4meClientTestCommon.test_port),
            app_type=OIDCApplicationType.native,
            client_name='Test',
            preferred_client_id='test_python_client',
            logo_url='http://{}:{}/logo.png'.format(TestID4meClientTestCommon.test_host, TestID4meClientTestCommon.test_port),
            policy_url='http://{}:{}/about'.format(TestID4meClientTestCommon.test_host, TestID4meClientTestCommon.test_port),
            tos_url='http://{}:{}/documents'.format(TestID4meClientTestCommon.test_host, TestID4meClientTestCommon.test_port),
            requireencryption=(params['requireencryption'] if 'requireencryption' in params else False),
            private_jwks_json=private_jwks_json if private_jwks_json is not None else (ID4meClient.generate_new_private_keys_set() if generate_private_keys else None),
            **kwargs
        )
